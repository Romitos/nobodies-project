/** @type {import('tailwindcss').Config} */
const {fontFamily} = require('tailwindcss/defaultTheme')

module.exports = {
    content: ['./src/**/*.{js,ts,jsx,tsx}', // Note the addition of the `app` directory.
    ], theme: {
        extend: {
            colors: {
                'root-primary': '#A259FF',
                'root-primary-darken': '#7f3bde',
                'root-background': '#2B2B2B',
                'root-background-secondary': '#3B3B3B',
                'root-label-text-color': '#858584',
                'root-white': '#FFFFFF',
                'root-footer-text-color': '#CCCCCC',
            },
            backgroundImage: {
                'root-gradient-1': 'linear-gradient(100.92deg, #A259FF 13.57%, #FF6250 97.65%)',
                'root-gradient-2': 'linear-gradient(128.15deg, #A259FF 49.75%, #377DF7 136.56%)',
            },
            height: {
                '18': '4.5rem'
            }
        },
        fontFamily: {
            'work-sans': ['var(--font-work-sans)', ...fontFamily.sans],
            'space-mono': ['var(--font-space-mono)', ...fontFamily.sans],
        },
        fontSize: {
            'sm': '0.75rem',
            'base': '1rem',
            'xl': '1.375rem',
            '2xl': '1.75rem',
            '3xl': '2.375rem',
            '4xl': '3.1875rem',
            '5xl': '4.1875rem',
        },
        lineHeight: {
            none: 1, tight: 1.1, snug: 1.2, normal: 1.4, relaxed: 1.6,
        }
    },
    plugins: [],
}
