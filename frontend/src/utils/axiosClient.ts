import axios, { AxiosResponse } from 'axios'


class AxiosClient {
  // like http://127.0.0.1:8000
  private _api = process.env.NEXT_PUBLIC_DOMAIN as string

  constructor() {
    if (!this._api) {
      throw new Error('NEXT_PUBLIC_DOMAIN is not setting up in .env')
    }
  }

  async get<T = any, D = any>(url: string, data?: D): Promise<AxiosResponse<T>> {
    return await axios.get(this._api + url, {data})
  }

  async post<T = any, D = any>(url: string, data?: D): Promise<AxiosResponse<T>> {
    return await axios.post(this._api + url, data)
  }
}

const client = new AxiosClient()

export const getAxiosClient = () => {
  if (typeof window === 'undefined') {
    return
  }

  return client
}

