export enum Meta {
    LOADING = 'loading',
    INITIAL = 'initial',
    ERROR = 'error',
    SUCCESS = 'success'
}
