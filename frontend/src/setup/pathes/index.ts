export const Routes = {
  MARKETPLACE: '/marketplace',
  ROOT: '/',
  NFT: '/nft',
  RANKINGS: '/rankings',
}
