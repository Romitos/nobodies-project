import { makeAutoObservable, reaction, runInAction } from 'mobx'
import { RootStore } from './RootStore'


export class LayoutStore {

  public root: RootStore
  private _registerIsOpen = false
  private _registerInDom = false

  constructor(root: RootStore) {
    this.root = root
    makeAutoObservable(this, {}, {autoBind: true})
    this._init()
  }

  get registerIsOpen() {
    return this._registerIsOpen
  }

  get registerInDom() {
    return this._registerInDom
  }

  openRegisterWindow() {
    this._registerInDom = true
    setTimeout(() => {
      runInAction(() => {
        this._registerIsOpen = true
      })
    }, 0)
  }

  closeRegisterWindow() {
    this._registerIsOpen = false
    setTimeout(() => {
      runInAction(() => {
        this._registerInDom = false
      })
    }, 1000)
  }

  _init() {
    reaction(
      () => this._registerIsOpen,
      (value) => {
        if (typeof window !== 'undefined') {

          if (value) {
            document.body.style.overflow = 'hidden'
          } else {
            document.body.style.overflow = 'auto'
          }
        }
      }
    )
  }
}
