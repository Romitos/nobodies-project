import { RootStore } from '../RootStore'
import { makeAutoObservable } from 'mobx'
import { User } from './types'


const USERNAME_KEY = 'username'
const getInitialUser = (): User => ({
  id: 0,
  username: '',
  firstName: '',
  lastName: '',
  token: ''
})

export class UserStore {
  public root: RootStore
  private _data: User = getInitialUser()

  constructor(root: RootStore) {
    this.root = root
    makeAutoObservable(this, {}, {autoBind: true})
    this._init()
  }

  setData(data: User) {
    this._saveUsername(data.username)
    this._data = data
  }

  get username() {
    return this._data.username
  }

  private _saveUsername(username: string): void {
    if (typeof window === 'undefined') {
      return
    }
    localStorage.setItem(USERNAME_KEY, username)
  }

  private _init() {
    this._hydrateUsername()
  }

  private _hydrateUsername(): void {
    if (typeof window === 'undefined') {
      return
    }
    const username = localStorage.getItem(USERNAME_KEY)

    if (username) {
      this._data.username = username
    }
  }
}
