import { LayoutStore } from './LayoutStore'
import { AuthStore } from './auth'
import { UserStore } from './user'
import { MarketStore } from './market'


export class RootStore {
  layoutStore: LayoutStore
  authStore: AuthStore
  userStore: UserStore
  marketStore: MarketStore

  constructor() {
    this.layoutStore = new LayoutStore(this)
    this.authStore = new AuthStore(this)
    this.userStore = new UserStore(this)
    this.marketStore = new MarketStore(this)
  }
}
