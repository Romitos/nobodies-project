import { RootStore } from '../RootStore'
import { makeAutoObservable } from 'mobx'


export class MarketStore {
  public root: RootStore
  private _activeState = 0

  constructor(root: RootStore) {
    this.root = root
    makeAutoObservable(this, {}, {autoBind: true})
  }

  setActiveState(number: number) {
    this._activeState = number
  }

  get activeState() {
    return this._activeState
  }

}
