export type RegisterAPIVariables = {
    username: string;
    first_name: string;
    last_name: string;
    password: string;
}

export type RegisterAPI = {

    id: 2147483647;
    username: string;
    first_name: string;
    last_name: string;
    is_active: true;
    is_approved: false;
    is_superuser: false;
    created_at: string;
    updated_at: string;
    full_name: string;

}

export type RegisterFields = {
    username: string;
    firstName: string;
    lastName: string;
    password: string;
}
