import { RootStore } from '../RootStore'
import { computed, makeAutoObservable, runInAction, toJS } from 'mobx'
import { RegisterAPI, RegisterAPIVariables, RegisterFields } from './types'
import { getAxiosClient, log, Meta } from 'src/utils'
import { User } from '../user/types'


const getInitialFields = (): RegisterFields => ({
  username: '',
  firstName: '',
  lastName: '',
  password: ''
})

export class AuthStore {
  public root: RootStore
  private _fields = getInitialFields()

  private _status: Meta = Meta.INITIAL

  constructor(root: RootStore) {
    this.root = root
    makeAutoObservable(this, {}, {autoBind: true})
  }

  async register(): Promise<void> {
    const client = getAxiosClient()
    if (client) {
      this._status = Meta.LOADING
      try {
        const {data} = await client.post<RegisterAPI, RegisterAPIVariables>('/users/register', prepareRegister(toJS(this._fields)))
        runInAction(() => {
          this.root.userStore.setData(normalizeUser(data))
          this._afterSuccessRegister()
        })

      } catch (e) {
        if (e instanceof Error) {
          log(e.message)
        }

        runInAction(() => {
          this._status = Meta.ERROR
        })
      }

    }
  }

  _afterSuccessRegister() {
    this._status = Meta.SUCCESS
    this.root.layoutStore.closeRegisterWindow()
  }

  getFieldValue(field: keyof RegisterFields) {
    return computed(() => this._fields[field]).get()
  }

  setFieldValue(field: keyof RegisterFields, value: string): void {
    this._fields[field] = value
  }


}

const prepareRegister = (data: RegisterFields): RegisterAPIVariables => ({
  first_name: data.firstName,
  last_name: data.lastName,
  password: data.password,
  username: data.username
})


const normalizeUser = (data: RegisterAPI): User => ({
  username: data.username,
  lastName: data.last_name,
  firstName: data.first_name,
  token: '',
  id: data.id
})
