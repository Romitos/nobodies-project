'use client'
import { ReactNode } from 'react'
import { RootStoreProvider } from './store-provider'


export const Providers = ({children}: { children: ReactNode }) => (
  <RootStoreProvider>
    {children}
  </RootStoreProvider>
)
