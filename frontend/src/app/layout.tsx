'use client'
import '../styles/global.css'
import { ReactNode } from 'react'
import { Space_Mono, Work_Sans } from '@next/font/google'
import { Container } from 'src/common'
import { Providers } from './providers'


const spaceMono = Space_Mono({
  subsets: ['latin'],
  weight: ['400', '700'],
  variable: '--font-space-mono'
})

const workSans = Work_Sans({
  subsets: ['latin'],
  weight: ['400', '700'],
  variable: '--font-work-sans'
})

export default function RootLayout({children}: { children: ReactNode }) {
  return (

    <html lang="ru">
      <body
        className={`${spaceMono.variable} ${workSans.variable} bg-root-background text-white`}
      >
        <Providers>
          <Container>
            {children}
          </Container>
        </Providers>
      </body>
    </html>

  )
}
