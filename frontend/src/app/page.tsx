'use client'
import '../setup/configureMobX'
import {enableStaticRendering} from 'mobx-react-lite'
import {Home} from 'src/modules'


enableStaticRendering(typeof window === 'undefined')

export default function HomePage() {
  return (
    <Home/>
  )
}
