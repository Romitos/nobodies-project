'use client'
import { enableStaticRendering } from 'mobx-react-lite'
import React, { createContext, ReactNode, useContext } from 'react'
import { RootStore } from '../setup/stores'


enableStaticRendering(typeof window === 'undefined')


let store: RootStore
const StoreContext = createContext<RootStore | undefined>(undefined)
StoreContext.displayName = 'StoreContext'

export function useRootStore() {
  const context = useContext(StoreContext)
  if (context === undefined) {
    throw new Error('useRootStore must be used within RootStoreProvider')
  }

  return context
}

export function useLayoutStore() {
  const {layoutStore} = useRootStore()
  return layoutStore
}

export function useAuthStore() {
  const {authStore} = useRootStore()
  return authStore
}

export function useUserStore() {
  const {userStore} = useRootStore()
  return userStore
}

export function useMarketStore() {
  const {marketStore} = useRootStore()
  return marketStore
}

export function RootStoreProvider({children}: { children: ReactNode }) {
  const rootStore = initializeStore()

  return (
    <StoreContext.Provider value={rootStore}>{children}</StoreContext.Provider>
  )
}

function initializeStore(): RootStore {
  const _store = store ?? new RootStore()
  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') {
    return _store
  }
  // Create the store once in the client
  if (!store) {
    store = _store
  }

  return _store
}
