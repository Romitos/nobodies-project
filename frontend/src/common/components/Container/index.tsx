import { PropsWithChildren } from 'react'
import { Navbar } from '../Navbar'
import { Footer } from '../Footer'


export const Container = ({children}: PropsWithChildren) => (
  <div className={'container mx-auto'}>
    <Navbar/>
    {children}
    <Footer/>
  </div>
)
