import NFTLogo from './icons/NFTLogo.svg'
import InstagramLogo from './icons/InstagramLogo.svg'
import DiscordLogo from './icons/DiscordLogo.svg'
import TwitterLogo from './icons/TwitterLogo.svg'
import YoutubeLogo from './icons/YoutubeLogo.svg'
import { Typography } from '../Typography'
import styles from './Footer.module.scss'
import { Button } from '../Button'


export const Footer = () => (
  <div className={styles.mainBlock}>
    <div className={styles.blocks}>
      <div className={'flex flex-col'}>
        <NFTLogo className={styles.logo}/>
        <Typography variant={'base'} className={'text-root-footer-text-color'}>NFT marketplace UI
          created <br/> with Anima for Figma.
        </Typography>
        <div className={'mt-8 mb-5'}>
          <Typography variant={'base'} className={'text-root-footer-text-color'}>
            Join our community
          </Typography>
        </div>
        <div className={styles.logos}>
          <DiscordLogo/>
          <YoutubeLogo/>
          <TwitterLogo/>
          <InstagramLogo/>
        </div>
      </div>
      <div className={'flex flex-col'}>
        <Typography variant={'h5'}>Explore</Typography>
        <div className={'flex flex-col mt-10 text-root-footer-text-color'}>
          <div>
            <button>Marketplace</button>
          </div>
          <div className={'my-5'}>
            <button>Ranking</button>
          </div>
          <div>
            <button>Connect a Wallet</button>
          </div>
        </div>
      </div>
      <div className={'flex flex-col'}>
        <div className={'mb-10'}>
          <Typography variant={'h5'}>Join Our Weekly Digest</Typography>
        </div>
        <Typography variant={'base'} className={'text-root-footer-text-color'}>get exclusive promotions &
          updates <br/> straight to your
          inbox
        </Typography>
        <div className={styles.subscribe}>
          <input className={styles.input} type="text" placeholder={'Enter your email here'}/>
          <Button
            variant={'contained'}
            size={'medium'}
            className={'-ml-10'}

          >
            Subscribe
          </Button>
        </div>
      </div>
    </div>
    <hr className={styles.divided}/>
    <div className={styles.copyright}>
      <Typography variant={'base'}>&copy; NFT Market. Use this template freely.</Typography>
    </div>
  </div>
)
