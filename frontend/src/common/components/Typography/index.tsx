import styles from './Typography.module.scss'
import cn from 'classnames'
import { DetailedHTMLProps, FC, HTMLAttributes } from 'react'


type TVariants = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'base' | 'caption'
type TFont = 'spaceMono' | 'workSans'
type TypographyProps = {
    variant: TVariants;
    font?: TFont;
} & DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>

export const Typography: FC<TypographyProps> = (props) => {

  const {variant, children, font = 'spaceMono', className, ...other} = props

  const fontStyle = font === 'spaceMono' ? styles.spaceMono : styles.workSans

  switch (variant) {
    case 'h1':
      return <h1 {...other} className={cn(styles.h1, fontStyle, className)}>{children}</h1>
    case 'h2':
      return <h2 {...other} className={cn(styles.h2, fontStyle, className)}>{children}</h2>
    case 'h3':
      return <h3 {...other} className={cn(styles.h3, fontStyle, className)}>{children}</h3>
    case 'h4':
      return <h4 {...other} className={cn(styles.h4, fontStyle, className)}>{children}</h4>
    case 'h5':
      return <h5 {...other} className={cn(styles.h5, fontStyle, className)}>{children}</h5>
    case 'base':
      return <p {...other} className={cn(styles.base, fontStyle, className)}>{children}</p>
    case 'caption':
      return <p {...other} className={cn(styles.caption, fontStyle, className)}>{children}</p>
  }
}
