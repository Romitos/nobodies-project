import styles from './Button.module.scss'
import { DetailedHTMLProps, HTMLAttributes } from 'react'
import cn from 'classnames'


type ButtonProps = {
  size?: 'small' | 'medium' | 'large';
  variant?: 'contained' | 'outlined';
  icon?: JSX.Element;
} & DetailedHTMLProps<HTMLAttributes<HTMLButtonElement>, HTMLButtonElement>

export const Button = (props: ButtonProps): JSX.Element => {
  const {size = 'medium', variant = 'contained', className, icon, children, ...other} = props
  return (
    <button
      {...other}
      className={cn(className, styles.button, {
        [styles.small]: size === 'small',
        [styles.medium]: size === 'medium',
        [styles.large]: size === 'large',
        [styles.contained]: variant === 'contained',
        [styles.outlined]: variant === 'outlined'
      })}
    >
      {icon}
      {children}
    </button>
  )
}
