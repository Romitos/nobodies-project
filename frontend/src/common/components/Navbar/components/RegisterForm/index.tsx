import Image from 'next/image'

import RegisterFormImage from './images/RegisterImage.png'
import User from './icons/User.svg'
import Email from './icons/Email.svg'
import LockKey from './icons/LockKey.svg'
import styles from './RegisterForm.module.scss'
import cn from 'classnames'
import { observer } from 'mobx-react-lite'
import { useAuthStore, useLayoutStore } from 'src/app/store-provider'
import { Button } from '../../../Button'


export const RegisterForm = observer(() => {

  const {registerIsOpen, closeRegisterWindow} = useLayoutStore()
  const {setFieldValue, getFieldValue, register} = useAuthStore()
  const handleClose = () => {
    closeRegisterWindow()
  }

  const handleClick = async () => {
    await register()
  }

  return (
    <div className={cn(styles.root, {
      [styles.open]: registerIsOpen,
      [styles.close]: !registerIsOpen
    })}
    >
      <Button
        size={'small'}
        className={styles.closeBtn}
        onClick={handleClose}
      >
        Close
      </Button>
      <div className={styles.left}>
        <Image
          className={styles.image}
          src={RegisterFormImage}
          alt={'Register Form Image'}
        />
      </div>
      <div className={styles.right}>
        <div>
          <h2 className="text-5xl font-bold">Create Account</h2>
          <p className="my-5">
            Welcome! Enter Your Details And Start <br/> Creating, Collecting
            And Selling NFTs.
          </p>
        </div>

        <div className={styles.form}>
          <div>
            <User/>
            <input
              type="text"
              name="Username"
              placeholder="Username"
              value={getFieldValue('username')}
              onChange={(e) => setFieldValue('username', e.target.value)}
            />
          </div>
          <div>
            <Email/>
            <input
              type="text"
              name="firstName"
              placeholder="First Name"
              value={getFieldValue('firstName')}
              onChange={(e) => setFieldValue('firstName', e.target.value)}
            />
          </div>
          <div>
            <LockKey/>
            <input
              type="text"
              name="lastName"
              placeholder="Last Name"
              value={getFieldValue('lastName')}
              onChange={(e) => setFieldValue('lastName', e.target.value)}
            />
          </div>
          <div>
            <LockKey/>
            <input
              type="password"
              name="Password"
              placeholder="Password"
              value={getFieldValue('password')}
              onChange={(e) => setFieldValue('password', e.target.value)}
            />
          </div>
          <div>
            <Button
              size={'small'}
              className={'w-full justify-center'}
              onClick={handleClick}
            >
              Create Account
            </Button>
          </div>
        </div>
      </div>
    </div>
  )
})
