import NFTIcon from './icons/Logo.svg'
import UserIcon from './icons/User.svg'
import styles from './Navbar.module.scss'
import { Typography } from '../Typography'
import { useLayoutStore } from 'src/app/store-provider'
import { RegisterForm } from './components'
import { observer } from 'mobx-react-lite'
import Link from 'next/link'
import { Routes } from '../../../setup'
import { Button } from '../Button'


export const Navbar = observer(() => {

  const {openRegisterWindow, registerInDom} = useLayoutStore()

  const handleOpen = () => {
    openRegisterWindow()
  }

  return (
    <>
      <div className={'h-24 flex justify-between items-center'}>
        <Link href={Routes.ROOT}>
          <NFTIcon width={243} height={32}/>
        </Link>
        <div className={'flex gap-1 items-center'}>
          <Link href={Routes.MARKETPLACE} className={'py-3 px-5'}>Marketplace</Link>
          <Link href={Routes.RANKINGS} className={'py-3 px-5'}>Rankings</Link>

          <Button variant={'contained'} icon={<UserIcon/>} onClick={handleOpen}>
            <Typography
              variant={'base'}
              font={'workSans'}
              className={styles.signUpButton}
            >
              {'Sign up'}
            </Typography>
          </Button>
        </div>
      </div>
      {registerInDom && <RegisterForm/>}
    </>
  )
})
