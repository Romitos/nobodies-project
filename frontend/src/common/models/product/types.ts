export interface Product {
  id: number;
  name: string;
  category_id: number;
  creator_id: number;
  owner_id: number;
  content: string;
  price: number;
  highest_bid: number;
  created_at: Date;
  updated_at: Date;
}

export type ProductsAPI = Product[]
