import { Typography } from '../../../common/components/Typography'
import SearchIcon from './icons/Search.svg'
import styles from './Marketplace.module.scss'
import { Tabs } from './components/Tabs'
import { CollectionCards, NftCards } from './components'
import { useMarketStore } from '../../../app/store-provider'
import { observer } from 'mobx-react-lite'
import { AnimatePresence, motion } from 'framer-motion'


const initialTabs = [
  'NFTs',
  'Collections',
]

export const Marketplace = observer(() => {
  const {activeState} = useMarketStore()
  return (
    <div className={styles.mainBlock}>
      <Typography variant={'h2'}>Browse Marketplace</Typography>

      <Typography variant={'base'}>Browse through more than 50k NFTs on the NFT Marketplace.</Typography>

      <div className={'mt-10 mb-20'}>
        <form>
          <div className={'relative '}>
            <input
              type={'search'} placeholder={'Search your favourite NFTs'}
              className={'w-full py-2 pl-3 pr-10 text-white bg-root-background rounded-3xl outline-none border border-root-background-secondary '}
            />
            <span className={'absolute inset-y-0 right-0 flex items-center pr-10'}>
              <button type={'submit'} className={'absolute p-1'}>
                <SearchIcon/>
              </button>
            </span>
          </div>
        </form>
      </div>
      <hr className={'styles.divided'}/>
      <Tabs
        initialTabs={initialTabs}
      />
      <AnimatePresence exitBeforeEnter>
        <motion.div
          key={activeState ? initialTabs[activeState] : 'empty'}
          initial={{y: 10, opacity: 0}}
          animate={{y: 0, opacity: 1}}
          exit={{y: -10, opacity: 0}}
          transition={{duration: 0.4}}
        >
          {activeState === 0 && <NftCards/>}
          {activeState === 1 && <CollectionCards/>}
        </motion.div>
      </AnimatePresence>
      <div>

      </div>
    </div>
  )
})


