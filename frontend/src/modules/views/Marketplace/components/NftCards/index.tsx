import { NftCardItem } from './components'
import AvatarSVG from 'src/modules/views/Marketplace/icons/Avatar.svg'
import { useEffect, useState } from 'react'
import { getAxiosClient, log } from 'src/utils'
import { Product, ProductsAPI, URL_GET_PRODUCT } from 'src/common/models/product'
import { Typography } from '../../../../../common/components/Typography'


export const NftCards = () => {

  const [products, setProducts] = useState<null | Product[]>(null)

  useEffect(() => {
    (async () => {
      try {
        const client = getAxiosClient()
        if (client) {
          const response = await client.get<ProductsAPI>(URL_GET_PRODUCT)
          const productResponse = response.data
          log(productResponse)
          setProducts(() => productResponse)
        }
      } catch (e) {
        log(e)
      }
    })()
  }, [])

  return (
    <div className={'grid grid-cols-3 grid-row-4 gap-4'}>
      {(products && products.length > 0) ? products.map((p, index) => (
        <NftCardItem
          index={index}
          key={p.id}
          src={`http://${p.content}`}
          alt={p.name}
          price={p.price}
          bid={p.highest_bid}
          title={p.name}
          userName={'Super User'}
          SVGIcon={AvatarSVG}
        />
      )) : <Typography variant={'h2'}>Loading...</Typography>}
    </div>
  )
}


