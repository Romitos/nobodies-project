import { motion } from 'framer-motion'
import Image from 'next/image'
import { type StaticImageData } from 'next/image'
import { FC, memo } from 'react'


type TCardItemProps = {

  src: StaticImageData | string;
  alt: string;
  price: string | number;
  bid: string | number;
  title: string;
  userName: string;
  SVGIcon: FC<any>;
  index: number;
};
export const NftCardItem = memo(({src, alt, price, bid, title, userName, SVGIcon, index}: TCardItemProps) => (
  <motion.div
    layout
    className="shadow-xl w-80 rounded-3xl overflow-hidden bg-root-background-secondary"
    initial={{
      opacity: 0,
      translateX: index % 2 === 0 ? -50 : 50,
      translateY: -50,
    }}
    animate={{
      opacity: 1,
      translateX: 0,
      translateY: 0
    }}
    transition={{
      duration: 0.3,
      delay: 0.2 * index
    }}
  >
    <Image src={src} alt={alt} width={330} height={296}/>

    <div>
      <div className="ml-4 mt-2">
        <p>{title}</p>
      </div>
      <div className="flex flex-row justify-start">
        <div className="ml-4 mt-2 mb-6">
          <SVGIcon/>
        </div>
        <div className="ml-1.5 mt-2">
          <small>{userName}</small>
        </div>
      </div>
    </div>


    <div className="flex flex-row justify-between">
      <div className="ml-3.5">
        <p className="text-xs text-root-label-text-color">Price</p>
        <p>{price} ETH</p>
      </div>
      <div className="mr-3.5 mb-2.5">
        <p className="text-xs text-root-label-text-color">Highest Bid</p>
        <p>{bid} wETH</p>
      </div>
    </div>
  </motion.div>
))

NftCardItem.displayName = 'NftCardItem'
