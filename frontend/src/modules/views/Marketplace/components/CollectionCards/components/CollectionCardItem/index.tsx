import Image from 'next/image'

import { type StaticImageData } from 'next/image'
import { FC, memo } from 'react'


import styles from '../../../../Marketplace.module.scss'
import SecondPhoto from '../../../../../Home/components/TrendingCollection/icons/Secondary Photo Placeholder.png'
import ThirdPhoto from '../../../../../Home/components/TrendingCollection/icons/Third Photo Placeholder.png'
import { motion } from 'framer-motion'


type TCardItemProps = {

  src: StaticImageData;
  alt: string;
  title: string;
  userName: string;
  amount: string;
  SVGIcon: FC<any>;
  index: number;
};

export const CollectionCardItem = memo(({src, alt, title, userName, amount, SVGIcon, index}: TCardItemProps) => (
  <motion.div
    layout
    initial={{
      opacity: 0,
      translateX: index % 2 === 0 ? -50 : 50,
      translateY: -50,
    }}
    animate={{
      opacity: 1,
      translateX: 0,
      translateY: 0
    }}
    transition={{
      duration: 0.3,
      delay: 0.2 * index
    }}
  >
    <div>
      <Image
        src={src}
        alt={alt}
        width={330}
        height={330}
      />
    </div>
    <div className={styles.smallCards}>
      <div>
        <Image
          src={SecondPhoto}
          alt={'Second Photo'}
          width={100}
          height={100}
        />
      </div>
      <div className={'mr-3 ml-3'}>
        <Image
          src={ThirdPhoto}
          alt={'ThirdPhoto'}
          width={100}
          height={100}
        />
      </div>
      <button className={styles.button}>{amount}</button>
    </div>
    <div className={styles.collectionName}>
      <h4>{title}</h4>
    </div>
    <div>
      <figure className={styles.userBlock}>
        <SVGIcon width={24} height={24}/>
        <figcaption className={styles.userName}>{userName}</figcaption>
      </figure>
    </div>
  </motion.div>
))

CollectionCardItem.displayName = 'CollectionCardItem'
