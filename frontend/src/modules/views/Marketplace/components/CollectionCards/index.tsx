import { CollectionCardItem } from './components'
import AgePicture from 'src/modules/views/Marketplace/images/Ape.png'
import AvatarSVG from 'src/modules/views/Marketplace/icons/Avatar.svg'
import { useEffect, useState } from 'react'
import { Typography } from '../../../../../common/components/Typography'


export const CollectionCards = () => {

  const [collections, setCollections] = useState<null | number[]>(null)

  useEffect(() => {
    setTimeout(() => {
      const newCollections = new Array(12).fill(1).map((el: number, index) => el + index)
      setCollections(() => newCollections)
    }, 15)
  }, [])

  return (
    <div className={'grid grid-cols-3 grid-row-4 gap-4'}>
      {(collections && collections.length > 0) ? collections.map((value, index) => (
        <CollectionCardItem
          index={index}
          key={value}
          src={AgePicture}
          alt={'age picture'}
          title={'Distant Galaxy'}
          userName={'Animakid'}
          amount={'432'}
          SVGIcon={AvatarSVG}
        />
      )) : <Typography variant={'h2'}>Loading...</Typography>}
    </div>
  )
}
