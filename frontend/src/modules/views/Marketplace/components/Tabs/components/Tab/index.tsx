import { Typography } from 'src/common/components/Typography'
import { ReactNode } from 'react'
import styles from './Tab.module.scss'
import cn from 'classnames'


type TabProps = {
  children: ReactNode;
  isActive?: boolean;
  onClick?: () => void;
}

export const Tab = ({children, isActive, onClick}: TabProps) => (
  <div
    onClick={onClick}
    className={cn(styles.root)}
  >
    <Typography variant={'h5'} className={cn(styles.text, {
      [styles.activeText]: isActive
    })}
    >
      {children}
    </Typography>
  </div>
)
