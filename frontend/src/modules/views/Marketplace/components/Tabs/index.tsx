import styles from './Tabs.module.scss'
import { Tab } from './components'
import { observer } from 'mobx-react-lite'
import { useMarketStore } from '../../../../../app/store-provider'
import { motion } from 'framer-motion'


type TabsProps = {
  initialTabs: string[];

}

export const Tabs = observer(({initialTabs}: TabsProps) => {

  const {setActiveState, activeState} = useMarketStore()

  return (
    <div className={styles.root}>
      {initialTabs.map((text, index) => (
        <Tab
          key={text}
          onClick={() => setActiveState(index)}
          isActive={activeState === index}
        >
          {text}
          {index === activeState ? (
            <motion.div className={styles.underline} layoutId="underline"/>
          ) : null}
        </Tab>
      ))}

    </div>
  )
})
