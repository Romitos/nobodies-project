import Image, { type StaticImageData } from 'next/image'
import { Typography } from '../../../../../../../common/components/Typography'


type TCardItemProps = {
  src: StaticImageData;
  alt: string;
  title: string;
  info: string;
};
export const CardItem = ({src, alt, title, info}: TCardItemProps) => (

  <div>
    <div className="w-80 h-150 bg-root-background-secondary rounded-3xl flex flex-col items-center">
      <div>
        <Image src={src} alt={alt} width={250} height={250}/>
      </div>
      <div className="my-5">
        <Typography variant={'h5'}>{title}</Typography>

      </div>
      <div className="w-64 h-24 text-center mb-5">
        <Typography variant={'base'}>{info}</Typography>

      </div>
    </div>
  </div>

)
