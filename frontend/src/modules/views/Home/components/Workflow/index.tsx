import styles from '../DiscoverNFT/DiscoverNFT.module.scss'
import {CardItem} from './components'

import WalletImage from './images/Wallet.png'
import CollectionImage from './images/Collection.png'
import CartImage from'./images/Cart.png'
import {Typography} from '../../../../../common/components/Typography'


export const Workflow = () => (
  <div className="px-20">
    <div className={styles.textBlock}>
      <Typography variant={'h3'}>How it works</Typography>
      <Typography variant={'base'}>Find Out How To Get Started</Typography>


    </div>
    <div className="flex justify-between items-center">
      <CardItem src={WalletImage} alt={'Wallet'} title={'Setup Your wallet'} info={'Set up your wallet of choice. Connect it to the Animarket by clicking the wallet icon in the top right corner.'}/>
      <CardItem src={CollectionImage} alt={'Collection'} title={'Create Collection'} info={'Upload your work and setup your collection. Add a description, social links and floor price.'}/>
      <CardItem src={CartImage} alt={'Cart'} title={'Start Earning'} info={'Choose between auctions and fixed-price listings. Start earning by selling your NFTs or trading others.'}/>

    </div>

  </div>
)
