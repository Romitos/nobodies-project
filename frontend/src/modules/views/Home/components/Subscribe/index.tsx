import Image from 'next/image'

import Photo from './images/Photo.png'
import EmailIcon from './icons/EnvelopeSimple.svg'
import {Typography} from '../../../../../common/components/Typography'
import { Button } from '../../../../../common'

import styles from'./Subscribe.module.scss'


export const Subscribe = () => (
  <div className={styles.mainBlock}>
    <div className={styles.image}>
      <Image src={Photo} alt={'Astronaut'} width={425} height={310}/>
    </div>
    <div className={'mr-10'}>
      <Typography variant={'h3'}>JOIN OUR WEEKLY <br/> DIGEST</Typography>
      <Typography variant={'base'}>Get Exclusive Promotions & Updates <br/> Straight To Your Inbox.</Typography>
      <div className={'flex flex-row mt-10'}>
        <input className={styles.input} type='text' placeholder={'Enter your email here'}/>

        <Button
          variant={'contained'}
          size={'medium'}
          icon={<EmailIcon/>}
          className={'-ml-10'}
        >Subscribe
        </Button>

      </div>
    </div>


  </div>
)
