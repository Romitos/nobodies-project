import Image from 'next/image'
import MainPhoto from './icons/Primary Photo Placeholder.png'
import SecondPhoto from './icons/Secondary Photo Placeholder.png'
import ThirdPhoto from './icons/Third Photo Placeholder.png'
import Avatar from './icons/Avatar Placeholder.png'
import Photo2 from './icons/Primary2.png'
import Photo3 from './icons/Primary3.png'
import Avatar2 from './icons/Avatar2.png'
import Avatar3 from './icons/Avatar3.png'

import styles from './TrendingCollection.module.scss'
import Link from 'next/link'
import { Routes } from 'src/setup'


export const TrendingCollection = () => (
  <div className="px-20">
    <div className={styles.textBlock}>
      <h2 className={styles.h2}>Trending Collection</h2>
      <p>Checkout Our Weekly Updated Trending Collection.</p>
    </div>
    <div className={styles.cards}>
      <div>
        <div>
          <Link href={`${Routes.NFT }?productID=superPuperIDDDD`}>
            <Image
              src={MainPhoto}
              alt={'Main Photo'}
              width={330}
              height={330}
            />
          </Link>
        </div>
        <div className={styles.smallCards}>
          <div>
            <Image
              src={SecondPhoto}
              alt={'Second Photo'}
              width={100}
              height={100}
            />
          </div>
          <div>
            <Image
              src={ThirdPhoto}
              alt={'ThirdPhoto'}
              width={100}
              height={100}
            />
          </div>
          <button className={styles.button}>1025+</button>
        </div>
        <div className={styles.collectionName}>
          <h4>DSGN Animals</h4>
        </div>
        <div>
          <figure className={styles.userBlock}>
            <Image src={Avatar} alt={'Avatar'} width={24} height={24}/>
            <figcaption className={styles.userName}>MrFox</figcaption>
          </figure>
        </div>
      </div>
      <div>
        <div>
          <Image src={Photo2} alt={'Main Photo'} width={330} height={330}/>
        </div>
        <div className={styles.smallCards}>
          <div>
            <Image
              src={Photo2}
              alt={'Second Photo'}
              width={100}
              height={100}
            />
          </div>
          <div className="px-4">
            <Image src={Photo2} alt={'ThirdPhoto'} width={100} height={100}/>
          </div>
          <button className={styles.button}>1025+</button>
        </div>
        <div className={styles.collectionName}>
          <h4>Magic Mushrooms</h4>
        </div>
        <div>
          <figure className={styles.userBlock}>
            <Image src={Avatar2} alt={'Avatar'} width={24} height={24}/>
            <figcaption className={styles.userName}>Shroomie</figcaption>
          </figure>
        </div>
      </div>
      <div>
        <div>
          <Image src={Photo3} alt={'Main Photo'} width={330} height={330}/>
        </div>
        <div className={styles.smallCards}>
          <div>
            <Image
              src={Photo3}
              alt={'Second Photo'}
              width={100}
              height={100}
            />
          </div>
          <div className="px-4">
            <Image src={Photo3} alt={'ThirdPhoto'} width={100} height={100}/>
          </div>
          <button className={styles.button}>1025+</button>
        </div>
        <div className="mb-2">
          <h4>Disco Machines</h4>
        </div>
        <div>
          <figure className={styles.userBlock}>
            <Image src={Avatar3} alt={'Avatar'} width={24} height={24}/>
            <figcaption className={styles.userName}>BeKind2Robots</figcaption>
          </figure>
        </div>
      </div>
    </div>
  </div>
)
