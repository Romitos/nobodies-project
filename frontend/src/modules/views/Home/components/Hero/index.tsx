import RocketIcon from './icons/Rocket.svg'
import Avatar from './icons/Avatar.svg'
import HeroImg from './images/HeroImage.png'
import Image from 'next/image'
import { useLayoutStore } from 'src/app/store-provider'
import { Button } from 'src/common'


export const Hero = () => {

  const {openRegisterWindow} = useLayoutStore()

  return (
    <div className={'flex flex-row justify-between items-center mx-20'}>
      <div className="mt-20">
        <div>
          <h1 className="text-5xl font-bold">
            Discover <br/> Digital art & <br/> Collect NFTs
          </h1>
          <p>
            NFT Marketplace UI Created With Anima For <br/> Figma. Collect, Buy
            And Sell Art From More <br/> Than 20k NFT Artists.
          </p>
        </div>
        <div className="mt-5">
          <Button
            variant={'contained'}
            size={'medium'}
            icon={<RocketIcon/>}
            onClick={openRegisterWindow}
          >
            Get Started
          </Button>
        </div>

        <div className={'flex justify-between items-center mt-5'}>
          <div>
            <p className={'font-bold'}>240k+</p>
            <small>Total Sale</small>
          </div>
          <div>
            <p className={'font-bold'}>100k+</p>
            <small>Auctions</small>
          </div>
          <div>
            <p className={'font-bold'}>240k+</p>
            <small>Artists</small>
          </div>
        </div>
      </div>

      <div className="shadow-xl md:w-2/5 w-full mt-20 rounded-md overflow-hidden bg-grayLight">
        <Image src={HeroImg} alt={'Hero Image'} width={510} height={401}/>

        <div>
          <div className="ml-4 mt-2">
            <p>Space Walking</p>
          </div>
          <div className="flex flex-row justify-start">
            <div className="ml-4 mt-2 mb-6">
              <Avatar/>
            </div>
            <div className="ml-1.5 mt-2">
              <small>Animakid</small>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
