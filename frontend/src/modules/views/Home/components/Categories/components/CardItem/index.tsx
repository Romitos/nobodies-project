import styles from '../../Categories.module.scss'
import Image, { type StaticImageData } from 'next/image'
import { FC } from 'react'


type TCardItemProps = {
  SVGIcon: FC<any>;
  src: StaticImageData;
  alt: string;
  title: string;
};

export const CardItem = ({src, alt, title, SVGIcon}: TCardItemProps) => (
  <div className={styles.card}>
    <div className="filter: blur relative">
      <Image src={src} alt={alt} width={240} height={240}/>
    </div>
    <div className={styles.icon}>
      <SVGIcon/>

    </div>

    <p className={styles.categoriesName}>{title}</p>
  </div>
)
