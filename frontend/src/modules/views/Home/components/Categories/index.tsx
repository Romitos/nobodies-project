import ArtImage from './images/Art.png'
import CollectiblesImage from './images/Collectibles.png'
import MusicImage from './images/Music.png'
import PhotographyImage from './images/Photography.png'
import VideoImage from './images/Video.png'
import UtilityImage from './images/Utility.png'
import SportImage from './images/Sport.png'
import VirtualWorlds from './images/VirtualWords.png'

import ArtIcon from './icons/PaintBrush.svg'
import CollectiblesIcon from './icons/Swatches.svg'
import MusicIcon from './icons/MusicNotes.svg'
import VideoIcon from './icons/VideoCamera.svg'
import UtilityIcon from './icons/MagicWand.svg'
import VirtualWorldsIcon from './icons/Planet.svg'
import SportIcon from './icons/Basketball.svg'


import styles from './Categories.module.scss'

import { CardItem } from './components'


export const Categories = () => (
  <div className="mt-20 mx-20">
    <div className={styles.title}>
      <h2 className={styles.h2}>Browse Categories</h2>
    </div>
    <div>
      <div className={styles.cards}>
        <CardItem src={ArtImage} alt={'Art'} title={'Art'} SVGIcon={ArtIcon}/>

        <CardItem
          src={CollectiblesImage}
          alt={'Collectibles'}
          title={'Collectibles'}
          SVGIcon={CollectiblesIcon}
        />
        <CardItem src={MusicImage} alt={'Music'} title={'Music'} SVGIcon={MusicIcon}/>
        <CardItem
          src={PhotographyImage}
          alt={'Photography'}
          title={'Photography'}
          SVGIcon={MusicIcon}
        />
        <CardItem src={VideoImage} alt={'Video'} title={'Video'} SVGIcon={VideoIcon}/>
        <CardItem src={UtilityImage} alt={'Utility'} title={'Utility'} SVGIcon={UtilityIcon}/>
        <CardItem src={SportImage} alt={'Sport'} title={'Sport'} SVGIcon={SportIcon}/>
        <CardItem
          src={VirtualWorlds}
          alt={'VirtualEorlds'}
          title={'Virtual Worlds'}
          SVGIcon={VirtualWorldsIcon}
        />
      </div>
    </div>
  </div>
)
