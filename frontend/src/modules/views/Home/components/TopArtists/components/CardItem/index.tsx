import styles from '../../TopArtists.module.scss'
import Image, { type StaticImageData } from 'next/image'


type TCardItemProps = {
  ranking: string;
  src: StaticImageData;
  alt: string;
  price: string;
  title: string;
};

export const CardItem = ({
  src,
  price,
  title,
  alt,
  ranking,
}: TCardItemProps) => (
  <div>
    <div className={styles.card}>
      <div className={styles.circle}>
        <p>{ranking}</p>
      </div>
      <div className={styles.avatar}>
        <Image src={src} alt={alt} width={120} height={120} />
      </div>
      <div>
        <h4>{title}</h4>
      </div>
      <div>
        <span className={styles.text}>Total Sales: </span>
        <span> {price} RTH</span>
      </div>
    </div>
  </div>
)
