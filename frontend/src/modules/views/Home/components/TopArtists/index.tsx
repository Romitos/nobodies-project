import styles from './TopArtists.module.scss'

import RocketIcon from './icons/RocketIcon.svg'
import Avatar from './icons/Avatar Placeholder.png'
import Avatar2 from './icons/Avatar2.png'
import Avatar3 from './icons/Avatar3.png'
import Avatar4 from './icons/Avatar4.png'
import Avatar5 from './icons/Avatar5.png'
import Avatar6 from './icons/Avatar6.png'
import Avatar7 from './icons/Avatar7.png'
import Avatar8 from './icons/Avatar8.png'
import Avatar9 from './icons/Avatar9.png'
import Avatar10 from './icons/Avatar10.png'
import Avatar11 from './icons/Avatar11.png'
import Avatar12 from './icons/Avatar12.png'
import { CardItem } from './components'
import { Button } from 'src/common'


export const TopArtists = () => (
  <div className="px-20">
    <div className="flex flex-row justify-between items-center">
      <div className={styles.textBlock}>
        <h2 className={styles.h2}>Top Creators</h2>
        <p>Checkout Top Rated Creators On The NFT Marketplace</p>
      </div>
      <div>
        <Button
          variant={'outlined'}
          icon={<RocketIcon/>}
        >
                        View Rankings
        </Button>
      </div>
    </div>
    <div className="grid overflow-hidden grid-cols-4 grid-rows-3 gap-2 gap-y-8">
      <CardItem
        src={Avatar}
        alt={'Avatar'}
        price={'34.53'}
        title={'Keepitreal'}
        ranking={'1'}
      />
      <CardItem
        src={Avatar2}
        alt={'Avatar'}
        price={'34.53'}
        title={'DigiLab'}
        ranking={'2'}
      />
      <CardItem
        src={Avatar3}
        alt={'Avatar'}
        price={'34.53'}
        title={'GravityOne'}
        ranking={'3'}
      />
      <CardItem
        src={Avatar4}
        alt={'Avatar'}
        price={'34.53'}
        title={'Juanie'}
        ranking={'4'}
      />
      <CardItem
        src={Avatar5}
        alt={'Avatar'}
        price={'34.53'}
        title={'BlueWhale'}
        ranking={'5'}
      />
      <CardItem
        src={Avatar6}
        alt={'Avatar'}
        price={'34.53'}
        title={'Mr Fox'}
        ranking={'6'}
      />
      <CardItem
        src={Avatar7}
        alt={'Avatar'}
        price={'34.53'}
        title={'Shroomie'}
        ranking={'7'}
      />
      <CardItem
        src={Avatar8}
        alt={'Avatar'}
        price={'34.53'}
        title={'Robotica'}
        ranking={'8'}
      />
      <CardItem
        src={Avatar9}
        alt={'Avatar'}
        price={'34.53'}
        title={'RustyRobot'}
        ranking={'9'}
      />
      <CardItem
        src={Avatar10}
        alt={'Avatar'}
        price={'34.53'}
        title={'Animakid'}
        ranking={'10'}
      />
      <CardItem
        src={Avatar11}
        alt={'Avatar'}
        price={'34.53'}
        title={'Dotgu'}
        ranking={'11'}
      />
      <CardItem
        src={Avatar12}
        alt={'Avatar'}
        price={'34.53'}
        title={'Ghiblier'}
        ranking={'12'}
      />
    </div>
  </div>
)
