import Image from 'next/image'
import { type StaticImageData } from 'next/image'
import { FC } from 'react'


type TCardItemProps = {

  src: StaticImageData;
  alt: string;
  price: string;
  bid: string;
  title: string;
  userName: string;
  SVGIcon: FC<any>;
};
export const CardItem = ({src, alt, price, bid, title, userName, SVGIcon}: TCardItemProps) => (
  <div className="shadow-xl w-80 h-1/3 rounded-3xl overflow-hidden bg-root-background-secondary">
    <Image src={src} alt={alt} width={330} height={296}/>

    <div>
      <div className="ml-4 mt-2">
        <p>{title}</p>
      </div>
      <div className="flex flex-row justify-start">
        <div className="ml-4 mt-2 mb-6">
          <SVGIcon/>
        </div>
        <div className="ml-1.5 mt-2">
          <small>{userName}</small>
        </div>
      </div>
    </div>


    <div className="flex flex-row justify-between">
      <div className="ml-3.5">
        <p className="text-xs text-root-label-text-color">Price</p>
        <p>{price} ETH</p>
      </div>
      <div className="mr-3.5 mb-2.5">
        <p className="text-xs text-root-label-text-color">Highest Bid</p>
        <p>{bid} wETH</p>
      </div>
    </div>
  </div>
)
