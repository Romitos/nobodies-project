import styles from './DiscoverNFT.module.scss'
import EyeIcon from './icons/Eye.svg'

import GalaxyImage from './images/Galxy.png'
import LifeImage from './images/Life.png'
import AstroImage from './images/Astro.png'
import Avatar from './icons/Avatar.svg'
import Avatar2 from './icons/Avatar2.svg'
import Avatar3 from './icons/Avatar3.svg'
import {CardItem} from './components'
import {Typography} from '../../../../../common/components/Typography'


export const DiscoverNFT = () => (
  <div className="px-20">
    <div className="flex flex-row justify-between items-center">
      <div className={styles.textBlock}>
        <Typography variant={'h3'}>Discover More NFTs</Typography>
        <Typography variant={'base'}>Explore New Trending NFTs</Typography>

      </div>
      <div>
        <button className={styles.button}>
          <div className="mr-1.5">
            <EyeIcon />
          </div>

                        View Rankings
        </button>
      </div>
    </div>
    <div className={styles.cards}>
      <CardItem src={GalaxyImage} alt={'Galaxy'} price={'1.63'} bid={'0.33'} title={'Distant Galaxy'} userName={'MoonDancer'} SVGIcon={Avatar}/>
      <CardItem src={LifeImage} alt={'Life'} price={'1.63'} bid={'0.33'} title={'Life On Edena'} userName={'NebulaKid'} SVGIcon={Avatar2}/>
      <CardItem src={AstroImage} alt={'Astro'} price={'1.63'} bid={'0.33'} title={'AstroFiction'} userName={'AstroFiction'} SVGIcon={Avatar3}/>
    </div>


  </div>
)
