import {Hero, TrendingCollection} from './components'
import {TopArtists} from './components/TopArtists'
import {Categories} from './components/Categories'
import {DiscoverNFT} from './components/DiscoverNFT'
import {Workflow} from './components/Workflow'
import {Subscribe} from './components/Subscribe'


export const Home = () => (
  <>
    <Hero/>
    <TrendingCollection/>
    <TopArtists/>
    <Categories/>
    <DiscoverNFT/>
    <Workflow/>
    <Subscribe/>
  </>
)
