'use client'
import Image from 'next/image'

import MainImage from './images/PlaceHolder.png'
import { Typography } from '../../../common/components/Typography'
import Avatar from './icons/Avatar.svg'
import Globe from './icons/Globe.svg'
import Arrow from './icons/ArrowRight.svg'
import { CardItem } from './components/CardItem'
import FoxImage from './images/Foxy.png'
import CatImage from './images/Cat.png'
import DogImage from './images/Dog.png'
import BearImage from './images/Bear.png'
import Robot1Image from './images/Robot1.png'
import Robot2Image from './images/Robot2.png'
import Robot3Image from './images/Robot3.png'
import Robot4Image from './images/Robot4.png'
import Robot5Image from './images/Robot5.png'


export const Nft = () => (
  <div>
    <Image src={MainImage} alt={'Main Image'} width={1280} height={560}/>
    <div className={'flex flex-row mt-5'}>
      <div className={'w-2/4'}>
        <div>
          <Typography variant={'h2'}>The Orbitians</Typography>
          <Typography variant={'base'} className={'text-root-label-text-color'}>Minted on Sep 30,
            2022
          </Typography>
        </div>

        <div className={'text-root-label-text-color mt-5'}>
          <Typography variant={'base'}>Created By</Typography>
        </div>
        <div className={'flex flex-row justify-start mt-3'}>
          <div>
            <Avatar/>
          </div>
          <div className={'ml-1.5'}>
            <Typography variant={'base'}>Orbitian</Typography>
          </div>
        </div>
        <div>
          <div className={'text-root-label-text-color mt-5'}>
            <Typography variant={'base'}>Description</Typography>
          </div>
          <div className={'mt-3'}>
            <Typography variant={'base'}>The Orbitians <br/>
              is a collection of 10,000 unique NFTs on the Ethereum <br/> blockchain,
            </Typography>
          </div>
          <div className={'mt-5'}>
            <Typography variant={'base'}>There are all sorts of beings in the NFT Universe.
              The <br/> most advanced and friendly of the bunch are Orbitians.
            </Typography>
          </div>
          <div className={'mt-5'}>
            <Typography variant={'base'}>
              They live in a metal space machines, high up in the sky <br/> and only have one foot on
              Earth. <br/>
              These Orbitians are a peaceful race, but they have been <br/> at war with a group of
              invaders for many generations. <br/> The invaders are called Upside-Downs, because of
              their <br/> inverted bodies that live on the ground, yet do not <br/> know any other way
              to be. Upside-Downs believe that <br/> they will be able to win this war if they could
              only get <br/> an eye into Orbitian territory, so they ve taken to make <br/> human
              beings their target.
            </Typography>
          </div>

          <div className={'text-root-label-text-color mt-5'}>
            <Typography variant={'base'}>Details</Typography>
          </div>

          <div className={'flex flex-row items-center mt-3'}>
            <div className={'mr-2'}>
              <Globe/>
            </div>

            <Typography variant={'base'}>View on Etherscan</Typography>
          </div>
          <div className={'flex flex-row items-center mt-3'}>
            <div className={'mr-2'}>
              <Globe/>
            </div>

            <Typography variant={'base'}>View Original</Typography>
          </div>

          <div className={'text-root-label-text-color mt-5'}>
            <Typography variant={'base'}>Tags</Typography>
          </div>

          <div className={'flex flex-row justify-between mt-3 w-2/4'}>
            <div>
              <button
                className={'flex bg-root-background-secondary py-5 px-3 rounded-3xl'}
              >ANIMATION
              </button>
            </div>
            <div className={'ml-2'}>
              <button
                className={'flex bg-root-background-secondary py-5 px-3 rounded-3xl'}
              >ILLUSTRATION
              </button>
            </div>
            <div className={'ml-2'}>
              <button className={'flex bg-root-background-secondary py-5 px-3 rounded-3xl'}>MOON
              </button>

            </div>
            <div className={'ml-2'}>
              <button className={'flex bg-root-background-secondary py-5 px-3 rounded-3xl'}>MOON
              </button>
            </div>


          </div>


        </div>
      </div>


      <div className={'w-72 h-56 bg-root-background-secondary rounded-3xl flex flex-col px-8 pt-5'}>
        <div>
          <Typography variant={'caption'}>Auction ends in:</Typography>
        </div>
        <div className={'flex flex-row justify-between'}>
          <div>
            <div>
              <Typography variant={'h3'}>59</Typography>
            </div>
            <div>
              <Typography variant={'caption'}>Hours</Typography>
            </div>


          </div>

          <div>
            <Typography variant={'h3'}>:</Typography>
          </div>


          <div>
            <div>
              <Typography variant={'h3'}>59</Typography>
            </div>
            <div>
              <Typography variant={'caption'}>Minutes</Typography>
            </div>
          </div>
          <div>
            <Typography variant={'h3'}>:</Typography>
          </div>


          <div>
            <div>
              <Typography variant={'h3'}>59</Typography>
            </div>
            <div>
              <Typography variant={'caption'}>Seconds</Typography>
            </div>
          </div>


        </div>

        <button
          className={'flex bg-root-primary w-56 py-4 pl-20 rounded-3xl hover:bg-purple-700 transition mt-8'}
        >Place
          Bid
        </button>
      </div>


    </div>

    <div className={'mt-20 mb-20 flex flex-row justify-between items-center'}>
      <div>
        <Typography variant={'h3'}>More from this artist</Typography>
      </div>
      <div>
        <button
          className={'flex items-center bg-root-background border-2 border-root-primary py-3 px-10 rounded-3xl hover:bg-purple-700 transition'}
        >
          <Arrow/>
          Go To Artist Page
        </button>
      </div>
    </div>
    <div className={'grid overflow-hidden grid-cols-3 grid-rows-3 gap-2 gap-y-8'}>
      <CardItem
        src={FoxImage}
        alt={'Foxy'}
        price={'1.63'}
        bid={'0.33'}
        title={'Foxy Life'}
        userName={'Orbitian'}
        SVGIcon={Avatar}
      />
      <CardItem
        src={CatImage} alt={'Cat'} price={'1.63'} bid={'0.33'} title={'Cat From Future'}
        userName={'Orbitian'} SVGIcon={Avatar}
      />
      <CardItem
        src={DogImage} alt={'Dog'} price={'1.63'} bid={'0.33'} title={'Psycho Dog'}
        userName={'Orbitian'} SVGIcon={Avatar}
      />
      <CardItem
        src={BearImage} alt={'Bear'} price={'1.63'} bid={'0.33'} title={'Psycho Dog'}
        userName={'Orbitian'} SVGIcon={Avatar}
      />
      <CardItem
        src={Robot1Image} alt={'Dog'} price={'1.63'} bid={'0.33'} title={'Psycho Dog'}
        userName={'Orbitian'} SVGIcon={Avatar}
      />
      <CardItem
        src={Robot2Image} alt={'Dog'} price={'1.63'} bid={'0.33'} title={'Psycho Dog'}
        userName={'Orbitian'} SVGIcon={Avatar}
      />
      <CardItem
        src={Robot3Image} alt={'Dog'} price={'1.63'} bid={'0.33'} title={'Psycho Dog'}
        userName={'Orbitian'} SVGIcon={Avatar}
      />
      <CardItem
        src={Robot4Image} alt={'Dog'} price={'1.63'} bid={'0.33'} title={'Psycho Dog'}
        userName={'Orbitian'} SVGIcon={Avatar}
      />
      <CardItem
        src={Robot5Image} alt={'Dog'} price={'1.63'} bid={'0.33'} title={'Psycho Dog'}
        userName={'Orbitian'} SVGIcon={Avatar}
      />
    </div>
  </div>
)
