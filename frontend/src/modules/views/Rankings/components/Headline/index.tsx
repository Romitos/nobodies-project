import styles from './Headline.module.scss'
import { Typography } from 'src/common/components/Typography'


export const Headline = () => (
  <div className={styles.root}>
    <Typography variant={'h2'} className={styles.head}>Top Creators</Typography>
    <Typography variant={'h5'}>
      Check out top ranking NFT artists on the NFT Marketplace.
    </Typography>
  </div>
)
