import styles from './Tabs.module.scss'
import { Tab } from './components'
import { useState } from 'react'
import { motion } from 'framer-motion'


const initialTabs = [
  'Today',
  'This Week',
  'This Month',
  'All Time'
]
export const Tabs = () => {
  const [activeTab, setActiveTab] = useState<number>(0)

  const handleClick = (value: number) => () => {
    setActiveTab(() => value)
  }
  return (
    <div className={styles.root}>
      {initialTabs.map((text, index) => (
        <Tab
          key={text}
          onClick={handleClick(index)}
          isActive={activeTab === index}
        >
          {text}
          {index === activeTab ? (
            <motion.div className={styles.underline} layoutId="underline"/>
          ) : null}
        </Tab>
      ))}

    </div>
  )

}
