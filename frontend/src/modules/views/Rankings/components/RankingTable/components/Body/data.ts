import { TableItemProps } from './components/TableItem'


type DataType = Array<TableItemProps>
export const data: DataType = [
  {
    index: 1,
    name: 'Keller Savage',
    change: 31.28,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 838,
    volume: 30.8
  },
  {
    index: 2,
    name: 'Nichole Kemp',
    change: -4.24,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 970,
    volume: 23
  },
  {
    index: 3,
    name: 'Chan Guzman',
    change: 15.88,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 410,
    volume: 94.7
  },
  {
    index: 4,
    name: 'Howe Underwood',
    change: -66.42,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 513,
    volume: 0.9
  },
  {
    index: 5,
    name: 'Knox Henry',
    change: -52.9,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 641,
    volume: 89.6
  },
  {
    index: 6,
    name: 'Rosetta Sargent',
    change: -13.38,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 477,
    volume: 50.8
  },
  {
    index: 7,
    name: 'Claudette Pratt',
    change: 49.63,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 592,
    volume: 11.2
  },
  {
    index: 8,
    name: 'Banks Salazar',
    change: -1.44,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 141,
    volume: 66.7
  },
  {
    index: 9,
    name: 'Whitley Harrell',
    change: 92.93,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 931,
    volume: 53
  },
  {
    index: 10,
    name: 'Stuart Mccullough',
    change: -55.11,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 682,
    volume: 75.7
  },
  {
    index: 11,
    name: 'Sanchez Harding',
    change: -95.17,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 593,
    volume: 66.2
  },
  {
    index: 12,
    name: 'Iris Conrad',
    change: 50.58,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 670,
    volume: 75.7
  },
  {
    index: 13,
    name: 'Valdez Kennedy',
    change: 86.64,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 287,
    volume: 49.1
  },
  {
    index: 14,
    name: 'Tami Riggs',
    change: -42.32,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 858,
    volume: 6
  },
  {
    index: 15,
    name: 'Short Noel',
    change: -27.75,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 421,
    volume: 92.4
  },
  {
    index: 16,
    name: 'Rosie Stout',
    change: 62.72,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 905,
    volume: 56.3
  },
  {
    index: 17,
    name: 'Reilly Evans',
    change: 78.93,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 915,
    volume: 11.3
  },
  {
    index: 18,
    name: 'Marisa Orr',
    change: -66.44,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 254,
    volume: 11.8
  },
  {
    index: 19,
    name: 'Fischer Whitehead',
    change: 30.3,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 753,
    volume: 73.6
  },
  {
    index: 20,
    name: 'Sheri Atkinson',
    change: 99.93,
    avatar: 'https://placekitten.com/60/60',
    nftSold: 794,
    volume: 94.7
  }
]
