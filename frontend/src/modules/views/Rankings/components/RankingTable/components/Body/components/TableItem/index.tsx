import styles from './TableItem.module.scss'
import { Typography } from 'src/common/components/Typography'
import Image from 'next/image'
import cn from 'classnames'


export type TableItemProps = {
  index: number;
  avatar: StaticImageData | string;
  name: string;
  change: number;
  nftSold: number;
  volume: number;
}

export const TableItem = (props: TableItemProps) => {
  const {index, change, nftSold, volume, name, avatar} = props
  return (
    <div className={styles.root}>
      <div className={styles.col1}>
        <Typography font={'workSans'} variant={'base'}>{index}</Typography>
      </div>
      <div className={styles.col2}>
        <Image className={styles.avatar} src={avatar} alt={`avatar for ${name}`} width={60} height={60}/>
        <Typography variant={'h5'}>{name}</Typography>
      </div>
      <div className={cn({
        [styles.red]: change < 0,
        [styles.green]: change > 0,
      })}
      >
        <Typography variant={'base'}>{change}%</Typography>
      </div>
      <div>
        <Typography variant={'base'}>{nftSold}</Typography>
      </div>
      <div>
        <Typography variant={'base'}>{volume} ETH</Typography>
      </div>
    </div>
  )
}
