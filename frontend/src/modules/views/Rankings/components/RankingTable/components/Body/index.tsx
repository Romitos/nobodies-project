import styles from './Body.module.scss'
import { data } from './data'
import { TableItem } from './components/TableItem'


export const Body = () => (
  <div className={styles.root}>
    {data.map((el) => <TableItem key={el.index} {...el}/>)}
  </div>
)
