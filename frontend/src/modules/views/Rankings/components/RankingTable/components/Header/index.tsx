import styles from './Header.module.scss'
import { Typography } from 'src/common/components/Typography'


export const Header = () => (
  <div className={styles.root}>
    <div className={'text-center'}>
      #
    </div>
    <div>
      <Typography variant={'base'}>Artist</Typography>
    </div>
    <div>
      <Typography variant={'base'}>Change</Typography>
    </div>
    <div>
      <Typography variant={'base'}>NFTs Sold</Typography>
    </div>
    <div>
      <Typography variant={'base'}>Volume</Typography>
    </div>
  </div>
)
