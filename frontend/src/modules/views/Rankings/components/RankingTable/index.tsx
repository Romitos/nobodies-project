import styles from './RankingTable.module.scss'
import { Body, Header } from './components'


export const RankingTable = () => (
  <div className={styles.root}>
    <Header/>
    <Body/>
  </div>
)
