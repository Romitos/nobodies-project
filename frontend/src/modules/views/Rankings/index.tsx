import { Headline, RankingTable, Tabs } from './components'


export const Rankings = () => (
  <div>
    <Headline/>
    <Tabs/>
    <RankingTable/>
  </div>
)
