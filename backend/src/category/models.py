from tortoise import fields, models


class Category(models.Model):
    """
    Модель категории
    """
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=20, unique=True)
