from fastapi import APIRouter, Depends
from fastapi.exceptions import HTTPException
from typing import List

from src.users.security import get_current_active_user
from src.category.models import Category
from src.category.schemas import CategoryPydantic, CategoryInPydantic


category_router = APIRouter(prefix="/category", tags=["category"])


@category_router.get('/', response_model=List[CategoryPydantic])
async def get_categories():
    """
    Получение всех категорий
    """
    return await Category.all()


@category_router.post('/create', response_model=CategoryPydantic)
async def create_category(category: CategoryInPydantic, user=Depends(get_current_active_user)):
    """
    Создание категории
    """
    category_obj = await Category.create(**category.dict(exclude_unset=True))
    return category_obj


@category_router.delete("/delete/{pk}", status_code=204)
async def delete_category(pk: int, user=Depends(get_current_active_user)):
    """
    Удаление категории
    """
    del_category = await Category.filter(id=pk).delete()
    if not del_category:
        raise HTTPException(status_code=404, detail=f"Category {pk} non found")


@category_router.put("/update/{pk}", response_model=CategoryInPydantic)
async def update_category(pk: int, data: CategoryInPydantic, user=Depends(get_current_active_user)):
    """
    Обновление категории
    """
    await Category.filter(id=pk).update(**data.dict(exclude_unset=True))
    return await Category.get(id=pk)
