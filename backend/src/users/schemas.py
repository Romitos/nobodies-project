from pydantic import BaseModel, Field


class UserUpdate(BaseModel):
    username: str
    first_name: str
    last_name: str


class UserRegister(UserUpdate):
    password: str = Field(min_length=8, max_length=20)


class Token(BaseModel):
    access_token: str
