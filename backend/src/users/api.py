from fastapi import APIRouter, HTTPException, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from tortoise.contrib.fastapi import HTTPNotFoundError

from .models import User_Pydantic, Users
from .schemas import Token, UserRegister, UserUpdate

from .hashing import get_hasher
from .security import authenticate_user, get_current_active_user, signJWT

users_router = APIRouter(prefix="/users", tags=["users"])


@users_router.post('/token', response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = await authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = signJWT(username=form_data.username)
    return access_token


@users_router.post("/register",
                   status_code=201,
                   response_model=User_Pydantic,
                   responses={404: {"model": HTTPNotFoundError}}
                   )
async def user_register(user: UserRegister):
    """
    Регистрация пользователя
    """
    password_hash = get_hasher()
    user.password = password_hash.hash(user.password)
    _user = await Users.get_or_none(username=user.username)
    if _user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"Пользователь {_user.username} уже существует"
        )
    new_user = await Users.create(**user.dict(exclude_unset=True))

    return await User_Pydantic.from_tortoise_orm(new_user)


@users_router.get("/me", status_code=200,
                  response_model=User_Pydantic,
                  responses={404: {"model": HTTPNotFoundError}}
                  )
async def get_me(current_user: Users = Depends(get_current_active_user)):
    """
    Получить информацию о текущем пользователе
    """
    return current_user


@users_router.get("/{pk}", status_code=200,
                  responses={404: {"model": HTTPNotFoundError}})
async def get_user_username(pk: int):
    """
    Получить юзернейм пользователя по id
    """
    user = await Users.get(id=pk)
    return {'username': user.username}


@users_router.put("/update",
                  status_code=200,
                  response_model=User_Pydantic,
                  responses={404: {"model": HTTPNotFoundError}}
                  )
async def update_user(user_data: UserUpdate, current_user: Users = Depends(get_current_active_user)):
    """
    Изменить профиль пользователя пользователя
    """
    await Users.filter(id=current_user.id).update(**user_data.dict(exclude_unset=True))
    return await User_Pydantic.from_queryset_single(Users.get(id=current_user.id))
