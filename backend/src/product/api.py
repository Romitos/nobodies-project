from datetime import datetime
import uuid
import os

from fastapi import APIRouter, Depends
from fastapi.exceptions import HTTPException
from typing import List

from src.product.models import Product
from src.users.models import Users
from src.category.models import Category
from src.users.security import get_current_active_user
from src.product.schemas import ProductPydantic, ProductInPydantic
from fastapi import UploadFile
from minio import Minio
from minio.error import S3Error
from fastapi.responses import Response
from config import site_config, minio_config


client = Minio(endpoint=f"{site_config.get('host')}:9000",
               access_key=minio_config.get('access_key'),
               secret_key=minio_config.get('secret_key'),
               secure=False)


try:
    found = client.bucket_exists("content")
    if not found:
        client.make_bucket("content")
    else:
        print("Bucket 'content' already exists")
except S3Error:
    print('Кошмар, произошла ошибка!')


product_router = APIRouter(prefix="/product", tags=["product"])


@product_router.get("/image/{filename}", responses={200: {"content": {"image/png": {}}}}, response_class=Response)
def get_image(filename: str):
    path = f'src/product/image_buffer/{filename}'
    client.fget_object('content', filename, path)
    with open(path, 'rb') as file:
        result = file.read()
    os.remove(path)
    return Response(content=result, media_type="image/png")


@product_router.post("/uploadfile/")
async def create_upload_file(pk: int, file: UploadFile):
    filename = f'{uuid.uuid4()}.png'
    path = f'src/product/image_buffer/{filename}'
    with open(path, 'wb') as out_file:
        content = await file.read()
        out_file.write(content)
    client.fput_object('content', filename, path)
    os.remove(path)

    product = await Product.get(id=pk)
    product.content = f'127.0.0.1:8000/product/image/{filename}'
    product.updated_at = datetime.now()
    await product.save()


@product_router.get('/', response_model=List[ProductPydantic])
async def get_products():
    """
    Получение всех продуктов
    """
    return await Product.all()


@product_router.get('/{pk}', response_model=List[ProductPydantic])
async def get_product_by_id(pk: int):
    """
    Получение продукта по id
    """
    return await Product.filter(id=pk)


@product_router.get('/by-category-id/{pk}', response_model=List[ProductPydantic])
async def get_products_by_category_id(pk: int):
    """
    Получение продуктов категории
    """
    return await Product.filter(category_id=pk)


@product_router.post('/create', status_code=200)
async def create_product(product: ProductInPydantic, user=Depends(get_current_active_user)):
    """
    Создание продукта
    """
    data = dict(product)
    category = await Category.get(id=data['category'])
    creator = await Users.get(id=data['creator'])
    owner = await Users.get(id=data['owner'])
    product = await Product.create(
        name=data['name'],
        category=category,
        creator=creator,
        owner=owner,
        price=data['price'],
        highest_bid=data['price']
    )
    await product.save()
    return {product}


@product_router.delete("/delete/{pk}", status_code=204)
async def delete_product(pk: int, user=Depends(get_current_active_user)):
    """
    Удаление продукта
    """
    del_product = await Product.filter(id=pk).delete()
    if not del_product:
        raise HTTPException(status_code=404, detail=f"Category {pk} non found")


@product_router.put("/update/{pk}", response_model=ProductPydantic)
async def update_product(pk: int, data: ProductInPydantic, user=Depends(get_current_active_user)):
    """
    Изменение продукта
    """
    await Product.filter(id=pk).update(**data.dict(exclude_unset=True))
    product = await Product.get(id=pk)
    product.updated_at = datetime.now()
    await product.save()
    return product
