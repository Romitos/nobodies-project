from tortoise import fields, models


class Product(models.Model):
    """
    Модель продукта
    """
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=20, unique=True)
    category = fields.ForeignKeyField('models.Category')
    creator = fields.ForeignKeyField('models.Users', related_name='creator')
    owner = fields.ForeignKeyField('models.Users', related_name='owner')
    content = fields.CharField(max_length=128, default='')
    price = fields.DecimalField(1000, 6)
    highest_bid = fields.DecimalField(1000, 6)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)
