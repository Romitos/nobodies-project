from pydantic import BaseModel
from datetime import datetime
from src.category.schemas import CategoryPydantic
from src.users.schemas import UserUpdate


class ProductPydantic(BaseModel):
    id: int
    name: str
    category_id: int
    creator_id: int
    owner_id: int
    content: str
    price: float
    highest_bid: float
    created_at: datetime
    updated_at: datetime


class ProductInPydantic(BaseModel):
    name: str
    category: int
    creator: int
    owner: int
    price: float
