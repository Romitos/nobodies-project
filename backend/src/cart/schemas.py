from pydantic import BaseModel


class CartPydantic(BaseModel):
    id: int
    user_id: int
    product_id: int
    count: int


class CartInPydantic(BaseModel):
    product_id: int
    count: int
