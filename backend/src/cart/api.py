from fastapi import APIRouter, Depends
from typing import List

from src.cart.schemas import CartPydantic, CartInPydantic
from src.users.security import get_current_active_user
from src.cart.models import Cart


cart_router = APIRouter(prefix="/cart", tags=["/cart"])


@cart_router.get('/', response_model=List[CartPydantic])
async def get_items_in_cart(user=Depends(get_current_active_user)):
    """
    Выдает коризну пользователя
    """
    return await Cart.filter(user_id=user.id)


@cart_router.post('/add/', status_code=200)
async def add_item_in_cart(data: CartInPydantic, user=Depends(get_current_active_user)):
    """
    Добавляет продукт и его количество
    """
    product_id, count = [el[1] for el in data]
    new_cart_item = await Cart.create(user_id=user.id, product_id=product_id, count=count)
    return new_cart_item


@cart_router.put('/change-count/')
async def change_count_of_item(data: CartInPydantic, user=Depends(get_current_active_user)):
    """
    Изменяет количество продукта в корзине
    """
    product_id, count = [el[1] for el in data]
    item = await Cart.get(user_id=user.id, product_id=product_id)
    item.count = count
    await item.save()
    return item


@cart_router.delete('/remove/{pk}')
async def remove_item_in_cart(pk: int):
    """
    Удаляет продукт из корзины
    """
    await Cart.filter(product_id=pk).delete()
