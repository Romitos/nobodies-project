from tortoise import fields, models


class Cart(models.Model):
    """
    Модель Корзины
    """
    id = fields.IntField(pk=True)
    user_id = fields.IntField()
    product_id = fields.IntField()
    count = fields.IntField()
