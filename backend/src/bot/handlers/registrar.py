from handlers.start import start_command, start_login, set_user_login, set_user_password, FSMLogin
from handlers.customer import show_customer_orders, show_order_full_text, show_performers_for_order
from handlers.performer import show_performer_orders


def register_handlers(dp):
    dp.register_message_handler(start_command, commands='start')
    dp.register_message_handler(start_login, commands='Войти', state=None)
    dp.register_message_handler(set_user_login, state=FSMLogin.login)
    dp.register_message_handler(set_user_password, state=FSMLogin.password)
    dp.register_message_handler(show_customer_orders, commands='Посмотреть_заказы')
    dp.register_callback_query_handler(show_order_full_text, text_contains='show_orders')
    dp.register_callback_query_handler(show_performers_for_order, text_contains='show_performers')
    dp.register_message_handler(show_performer_orders, commands='Показать_рекомендованные_заказы')
