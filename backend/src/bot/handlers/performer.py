from aiogram import types
from create_bot import bot
import time


orders = {1224805321: ['Условный заказ 1', 'Условный заказ 2', 'Условный заказ 3']}


# показать заказы для исполнения
async def show_performer_orders(message: types.Message):
    # тут должен быть условный запрос
    for order in orders[message.from_user.id]:
        await bot.send_message(message.from_user.id, order)
        time.sleep(0.5)
