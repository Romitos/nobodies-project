from aiogram import types
import json
from create_bot import bot
from keyboards.start import start_keyboard
from keyboards.customer import customer_keyboard
from keyboards.performer import performer_keyboard
from states.start import FSMLogin, FSMContext
from common.utils import request


login_users = {}


# стартовае приветствие на комманду /start
async def start_command(message: types.Message):
    await bot.send_message(message.from_user.id,
                           'Здравствуйте, это телеграмм-бот Профи.ру',
                           reply_markup=start_keyboard)


# начало состояния входа /Войти
async def start_login(message: types.Message):
    await FSMLogin.login.set()
    await message.reply('Введите логин')


# установка логина пользователя
async def set_user_login(message: types.Message, state: FSMContext):
    async with state.proxy() as user_credentials:
        user_credentials['login'] = message.text
    await FSMLogin.next()
    await message.reply('Теперь введите пароль')


# установка пароля пользователя и завершение состояния
async def set_user_password(message: types.Message, state: FSMContext):
    async with state.proxy() as user_credentials:
        user_credentials['password'] = message.text

    # условный запрос на получение токена
    data = json.dumps(dict(user_credentials))
    token = await request('http://127.0.0.1:5000/login/', data)
    if token:
        login_users[message.from_user.id] = token

    # условный запрос для получения пользователя и уточнение его роли
    data = json.dumps({'user_id': message.from_user.id, 'token': login_users[message.from_user.id]})
    user = await request('http://127.0.0.1:5000/users/', data)
    if user:
        user = json.loads(user)
        keyboard = customer_keyboard if user['role'] == 1 else performer_keyboard
        await bot.send_message(message.from_user.id, f'Вы успешно вошли, {user["name"]}',
                               reply_markup=keyboard)

    await state.finish()
