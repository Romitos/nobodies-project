from aiogram import types
import time
from create_bot import bot
from keyboards.customer import inline_customer_keyboard, full_text, performers, \
    show_orders_callback, show_performers_callback


# временные заглушки для тестирования
orders = {1224805321: ['Условный заказ 1', 'Условный заказ 2', 'Условный заказ 3']}
performers_list = [[1224805321, 79495235821, 'Данил', 'Каин'], [1224805321, 79495235821, 'Данил', 'Каин'],
                   [1224805321, 79495235821, 'Данил', 'Каин']]


# показать заказы заказчика
async def show_customer_orders(message: types.Message):
    # тут должен быть условный запрос на получение заказов
    for order in orders[message.from_user.id]:
        full_text.callback_data = show_orders_callback.new(order_id=1)
        performers.callback_data = show_performers_callback.new(order_id=1)
        await bot.send_message(message.from_user.id, order,
                               reply_markup=inline_customer_keyboard)
        time.sleep(0.5)


# показать полный текст заказа
async def show_order_full_text(call: types.CallbackQuery):
    await call.answer(cache_time=60)
    # получение параметра inline-кнопки some_id = call.data.split(':')[1]
    # тут должен быть запрос на получение текста
    await call.message.answer('Какой-то текст')


# показать исполнителей заказа
async def show_performers_for_order(call: types.CallbackQuery):
    await call.answer(cache_time=60)
    # тут должен быть запрос на получение исполнителей
    for contact in performers_list:
        await bot.send_contact(*contact)
        time.sleep(0.5)
