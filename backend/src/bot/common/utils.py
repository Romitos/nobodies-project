import aiohttp


async def request(address, data):
    async with aiohttp.ClientSession() as session:
        async with session.post(address, data=data) as resp:
            text = await resp.text() if resp.status == 200 else None
            await session.close()
            return text
