from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
import sys
sys.path.append('../../')
from config import bot_config


token = bot_config['token']
bot = Bot(token)
dp = Dispatcher(bot, storage=MemoryStorage())
