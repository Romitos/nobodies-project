from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State


class FSMLogin(StatesGroup):
    login = State()
    password = State()
