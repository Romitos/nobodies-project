from aiogram.utils import executor
from create_bot import dp, Dispatcher
from handlers import register_handlers


async def on_startup(dispatcher: Dispatcher):
    register_handlers(dispatcher)
    print('Бот запустился...')


executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
