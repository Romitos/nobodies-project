from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardMarkup
from aiogram.utils.callback_data import CallbackData
from aiogram.types import InlineKeyboardButton


show_orders_button = KeyboardButton('/Посмотреть_заказы')
customer_keyboard = ReplyKeyboardMarkup(resize_keyboard=True).add(show_orders_button)

show_orders_callback = CallbackData('show_orders', 'order_id')
show_performers_callback = CallbackData('show_performers', 'order_id')
full_text = InlineKeyboardButton(text='Посмотреть полный текст', callback_data='')
performers = InlineKeyboardButton(text='Показать имполнителей', callback_data='')
inline_customer_keyboard = InlineKeyboardMarkup(row_width=1)
inline_customer_keyboard.insert(full_text)
inline_customer_keyboard.insert(performers)
