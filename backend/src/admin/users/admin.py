from django.contrib import admin
from .models import Users, Product, Category, Cart


@admin.register(Users)
class AllUser(admin.ModelAdmin):
    readonly_fields = ("created_at", "updated_at", "id",)
    exclude = ['password']
    list_display = ('last_name', 'first_name',)


@admin.register(Product)
class AllProduct(admin.ModelAdmin):
    readonly_fields = ("created_at", "id",)


@admin.register(Category)
class AllProduct(admin.ModelAdmin):
    readonly_fields = ("id",)


@admin.register(Cart)
class AllProduct(admin.ModelAdmin):
    readonly_fields = ("id",)
