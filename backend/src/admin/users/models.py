from django.db import models


class Users(models.Model):
    """
    Модель пользователя
    """
    id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=20, unique=True, help_text="Введите свой ник")
    first_name = models.CharField(max_length=50, null=True, help_text="Введите свое имя")
    last_name = models.CharField(max_length=50, null=True, help_text="Введите свою фамилию")
    password = models.CharField(max_length=128, null=True)
    is_active = models.BooleanField(default=True)
    is_approved = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = "users"
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"
        ordering = ["-created_at"]


class Product(models.Model):
    """
    Модель продукта
    """
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=20, unique=True, help_text="Введите наименование продукта")
    category = models.ForeignKey('Category', on_delete=models.PROTECT)
    creator = models.ForeignKey('Users', related_name='creator', on_delete=models.PROTECT)
    owner = models.ForeignKey('Users', related_name='owner', on_delete=models.PROTECT)
    content = models.CharField(max_length=128, default='')
    price = models.DecimalField(max_digits=1000, decimal_places=6,)
    highest_bid = models.DecimalField(max_digits=1000, decimal_places=6,)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = "product"
        verbose_name = "Товар"
        verbose_name_plural = "Товары"


class Category(models.Model):
    """
    Модель категории
    """
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=20, unique=True, help_text="Введите наименование категории")

    class Meta:
        managed = False
        db_table = "category"
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class Cart(models.Model):
    """
    Модель Корзины
    """
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    product_id = models.IntegerField()
    count = models.IntegerField()

    class Meta:
        managed = False
        db_table = "cart"
        verbose_name = "Корзина"
        verbose_name_plural = "Корзины"