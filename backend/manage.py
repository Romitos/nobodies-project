import os

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from tortoise.contrib.fastapi import register_tortoise

from src.users import users_router
from src.category import category_router
from src.product import product_router
from src.cart import cart_router

from config import app_config, database_config, site_config, cors_config

app = FastAPI(**app_config, **cors_config)

register_tortoise(
    app=app,
    db_url=database_config.get('database_url').format(**database_config),
    modules={"models": ["src.users.models", "src.category.models", "src.product.models", "src.cart.models"]},
    generate_schemas=True,
    add_exception_handlers=True,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users_router)
app.include_router(category_router)
app.include_router(product_router)
app.include_router(cart_router)

if __name__ == "__main__":
    import uvicorn

    # os.system('gunicorn -b 127.0.0.1:8000 src.admin.configs.wsgi:application')
    uvicorn.run("manage:app", **site_config)
